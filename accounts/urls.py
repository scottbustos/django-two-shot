from django.urls import path
from accounts.views import account_login, account_logout, sign_up

urlpatterns = [
    path("login/", account_login, name="login"),
    path("logout/", account_logout, name="logout"),
    path("signup/", sign_up, name="signup"),
]
